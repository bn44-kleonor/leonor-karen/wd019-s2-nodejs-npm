//1) declare dependencies
const express = require("express");
const app = express();


//3) connect to db
const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/merng_tracker', {useNewUrlParser: true});

mongoose.connection.once("open", ()=> {
	console.log("Now connected to local MongoDB Server");
})

//2) initialize server
app.listen(4000, ()=> {
	console.log("Now listening for requests on port 4000");
});


