const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const memberSchema = new Schema(
		{
			firstname: { type: String, required: true },
			lastname: { type: String, required: true },
			position: String
		},
		{
			timestamps: true
		}
	);

module.exports = mongoose.model("Member", memberSchema);
